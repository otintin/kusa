VERSION=beta-v0.05

build:
	mkdir build
	cd build; cp -r ../overrides .;	cp ../manifest.json .; zip -r kusa-$(VERSION).zip .

.PHONY: clean

clean:
	rm -rf build
