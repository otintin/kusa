# kusa

kusa is a creative and building focused modpack for Minecraft 1.12.2.

## Installation
1. Import the release zip using your launcher of choice (GDLauncher preferred)
2. Download the additional content listed in extra.txt
3. Extract the [additional content](extra.md) into the appropriate folders
