# Additional content

## Required mods
- CashierMod (2.0.0)
  https://mukanote.com/download/cashiermod/
  Copy into mods/ folder
- Cocricot (0.3)
  https://cocricot.pics/#download
  Copy into mods/ folder and resourcepacks/ folder
- 路地裏 (0.1.3.a)
  Copy into mods/ folder and resourcepacks/ folder

## Required content
- Asphalt Mod Road Sign Pack
  https://mukanote.com/download/asphaltmod/
  Download the "道路標識パック (Road Sign Pack)"
  Unzip contents into mods/asphaltmod/roadsign/ folder

## Extra mods
- Optifine
- JourneyMap